import firebase from 'firebase/app'

import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'

const firebaseConfig = {
  apiKey: 'AIzaSyC5NQKlbnw6VLA9lg19-6HY0s7GMFNAQqY',
  authDomain: 'devter-7d665.firebaseapp.com',
  databaseURL: "https://devter-7d665.firebaseio.com",
  projectId: 'devter-7d665',
  storageBucket: 'devter-7d665.appspot.com',
  messagingSenderId: '542179097151',
  appId: '1:542179097151:web:c63d65228c2fd5b89f568a',
  measurementId: 'G-PMQ3M6GTVH',
}

!firebase.apps.length && firebase.initializeApp(firebaseConfig)

const db = firebase.firestore()

const mapUserFromFirebaseAuthToUser = (user) => {
  if (user === null) {
    return null
  } else {
    const { displayName, email, photoURL, uid } = user

    return {
      name: displayName,
      email,
      avatar: photoURL,
      uid,
    }
  }
}

export const onAuthStateChange = (onChange) => {
  return firebase.auth().onAuthStateChanged((user) => {
    const normalizerUser = mapUserFromFirebaseAuthToUser(user)
    onChange(normalizerUser)
  })
}

export const loginWithGitHub = () => {
  const githubProvider = new firebase.auth.GithubAuthProvider()
  return firebase.auth().signInWithPopup(githubProvider)
  // .then(mapUserFromFirebaseAuthToUser); //se activa sola despues de loguear
}

export const addDevit = ({ avatar, content, img, userId, name }) => {
  return db.collection('devits').add({
    avatar,
    content,
    img,
    userId,
    name,
    createdAt: firebase.firestore.Timestamp.fromDate(new Date()),
    likesCount: 0,
    sharedCount: 0,
  })
}

export const fetchLastestDevits = () => {
  return db
    .collection('devits')
    .orderBy('createdAt', 'desc')
    .get()
    .then((querySnapshot) => {
      return querySnapshot.docs.map((doc) => {
        const data = doc.data()
        const id = doc.id
        const { createdAt } = data

        return {
          id,
          ...data,
          createdAt: +createdAt.toDate(),
        }
      })
    })
}

export const uploadImage = (file) => {
  const ref = firebase.storage().ref(`images/${file.name}`)
  const task = ref.put(file)
  return task
}
