export const formatDate = (timestamp) => {
  const date = new Date(timestamp.seconds * 1000)
  
  const options = {
    year: 'numeric', month: 'long', day: 'numeric',
    hour: 'numeric', minute: 'numeric', second: 'numeric',
    hour12: false
  };
  const formattedTime = new Intl.DateTimeFormat('es-AR', options).format(date)
 
  return formattedTime.toString()
}
