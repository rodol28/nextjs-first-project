import Devit from 'components/Devit'

export default function DevitPage(props) {
  return (
    <>
      <section>
        <Devit {...props} />
      </section>
      <style jsx>{`
        section {
          margin-top: 20px;
          margin-left: 15px;
        }
      `}</style>
    </>
  )
}

export async function getServerSideProps(context) {
  const { params, res } = context
  const { id } = params

  const apiResponse = await fetch(`http://localhost:3000/api/devits/${id}`)
  if (apiResponse.ok) {
    const props = await apiResponse.json()
    return { props }
  }
  if (res) {
    res.writeHead(301, { location: '/home' }).end()
  }
}
