// react
import { useState, useEffect } from 'react'

// componentes
import Button from 'components/Button'

// custom hooks
import useUser from 'hooks/useUser'

// firebase client
import { addDevit, uploadImage } from 'firebase/client'

// nextjs
import { useRouter } from 'next/router'
import Head from 'next/head'
import Avatar from 'components/Avatar'

const COMPOSE_STATES = {
  USER_NOT_KNOWN: 0,
  LOADING: 1,
  SUCCESS: 2,
  ERROR: -1,
}

const DRAG_IMAGE_STATE = {
  ERROR: -1,
  NONE: 0,
  DRAG_OVER: 1,
  UPLOADING: 2,
  COMPLETE: 3,
}

export default function ComposeTweet() {
  const [message, setMessage] = useState('')
  const [status, setStatus] = useState(COMPOSE_STATES.USER_NOT_KNOWN)
  const [drag, setDrag] = useState(DRAG_IMAGE_STATE.NONE)
  const [task, setTask] = useState(null)
  const [imgURL, setImgURL] = useState(null)

  const user = useUser()
  const router = useRouter()

  useEffect(() => {
    if (task) {
      const onProgress = (snapshot) => {
        const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
        console.log('Upload is ' + progress + '% done')
      }
      const onError = (err) => {
        console.error(err)
      }
      const onComplete = () => {
        task.snapshot.ref.getDownloadURL().then(function (downloadURL) {
          console.log('File available at', downloadURL)
          setImgURL(downloadURL)
        })
      }

      task.on('state_changed', onProgress, onError, onComplete)
    }
  }, [task])

  const handleChange = (event) => {
    const { value } = event.target
    setMessage(value)
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    setStatus(COMPOSE_STATES.LOADING)

    addDevit({
      avatar: user.avatar,
      content: message,
      userId: user.uid,
      name: user.name,
      img: imgURL,
    })
      .then(() => {
        router.push('/home')
      })
      .catch((err) => {
        console.error(err)
        setStatus(COMPOSE_STATES.ERROR)
      })
  }

  const isButtonDesabled =
    message.length === 0 || status === COMPOSE_STATES.LOADING

  const handleDragEnter = (event) => {
    event.preventDefault()
    setDrag(DRAG_IMAGE_STATE.DRAG_OVER)
  }
  const handleDragLeave = (event) => {
    event.preventDefault()
    setDrag(DRAG_IMAGE_STATE.NONE)
  }
  const handleDrop = (event) => {
    event.preventDefault()
    setDrag(DRAG_IMAGE_STATE.NONE)
    const file = event.dataTransfer.files[0]

    const task = uploadImage(file)
    setTask(task)
  }

  return (
    <>
      <Head>
        <title>Crear un devit / Devter</title>
      </Head>
      <section className="form-container">
        {user && (
          <section className="avatar-container">
            <Avatar src={user.avatar} />
          </section>
        )}
        <form onSubmit={handleSubmit}>
          <textarea
            onChange={handleChange}
            onDragEnter={handleDragEnter}
            onDragLeave={handleDragLeave}
            onDrop={handleDrop}
            placeholder="¿Qué está pasando?"
          ></textarea>
          {imgURL && (
            <section className="remove-img">
              <button onClick={() => setImgURL(null)}>X</button>
              <img src={imgURL} />
            </section>
          )}
          <div>
            <Button disabled={isButtonDesabled}>Devitear</Button>
          </div>
        </form>
      </section>

      <style jsx>
        {`
          div {
            padding: 15px;
          }

          .form-container {
            align-items: flex-start;
            display: flex;
          }

          .avatar-container {
            padding-top: 20px;
            padding-left: 10px;
          }

          .remove-img {
            position: relative;
          }

          button {
            background: rgba(0, 0, 0, 0.3);
            border: 0;
            border-radius: 999px;
            color: #fff;
            font-size: 24px;
            width: 32px;
            height: 32px;
            top: 15px;
            position: absolute;
            right: 15px;
          }

          form {
            margin: 10px;
          }
          textarea {
            border: ${drag === DRAG_IMAGE_STATE.DRAG_OVER
              ? '3px dashed #09f'
              : '3px solid transparent'};
            border-radius: 10px;
            font-size: 21px;
            min-height: 200px;
            outline: 0;
            padding: 15px;
            resize: none;
            width: 100%;
          }
          img {
            border-radius: 10px;
            height: auto;
            width: 100%;
          }
        `}
      </style>
    </>
  )
}
