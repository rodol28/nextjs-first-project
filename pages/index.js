import Head from 'next/head'
import { useRouter } from 'next/router'
import { useEffect } from 'react'

// components
import Button from 'components/Button'
import GitHub from 'components/Icons/GitHub'
import Logo from 'components/Icons/Logo'

// styles
import { colors } from 'styles/theme'

// functions login github firebase
import { loginWithGitHub } from 'firebase/client'

// custom hooks
import useUser, { USER_STATES } from 'hooks/useUser'

export default function Home() {
  const user = useUser()
  const router = useRouter()

  useEffect(() => {
    user && router.replace('/home')
  }, [user])

  const handleClick = () => {
    loginWithGitHub().catch(function (error) {
      console.log(error.code, error.message)
    })
  }

  return (
    <>
      <Head>
        <title>Devter</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <section>
        <Logo width="100" />
        <h1>Devter</h1>
        <h2>
          Talk about development
          <br /> with developers 👨‍💻 👩‍💻
        </h2>
        <div>
          {user === USER_STATES.NOT_LOGGED && (
            <Button onClick={handleClick}>
              <GitHub fill="#fff" width={24} height={24} />
              Login with GitHub
            </Button>
          )}
          {user === USER_STATES.NOT_KNOWN && <img src="/spinner.gif" />}
        </div>
      </section>

      <style jsx>{`
        img {
          width: 120px;
        }

        div {
          margin: 16px;
        }

        section {
          display: grid;
          height: 100vh;
          place-content: center;
          place-items: center;
        }

        h1 {
          color: ${colors.primary};
          font-weight: 800;
          font-size: 30px;
          margin-bottom: 10px;
        }

        h2 {
          color: ${colors.secondary};
          font-size: 18px;
          margin: 0;
        }
      `}</style>
    </>
  )
}
